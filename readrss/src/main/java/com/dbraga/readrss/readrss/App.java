package com.dbraga.readrss.readrss;

import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import com.rometools.rome.feed.synd.SyndEntry;
import com.rometools.rome.feed.synd.SyndFeed;
import com.rometools.rome.io.SyndFeedInput;
import com.rometools.rome.io.XmlReader;

public class App {

    public static void main(String[] args) {
            
    	List<Feed> feedList = readFeed("http://feeds.feedburner.com/FastMotos?format=xml");
    	//List<Feed> feedList1 = readFeed("https://www.moto.com.br/rss/feed.xml");
        //List<Feed> feedList2 = readFeed("http://soumoto.com.br/feed/");
        
        for (Feed feed : feedList) {
        	System.out.println(feed.getUrl());
			System.out.println(feed.getPublishedDate());
			System.out.println(feed.getTitle());
			System.out.println(feed.getDescription());
			System.out.println("__________________________\n");
		}
    }

	private static List<Feed> readFeed(String url) {
		List<Feed> feedList = new ArrayList<>();
		try {
			URL feedUrl = new URL(url);
			SyndFeedInput input = new SyndFeedInput();
			SyndFeed feedData = input.build(new XmlReader(feedUrl));
			
			for (SyndEntry rss : feedData.getEntries()) {
				Feed feed = new Feed(rss.getUri(), rss.getPublishedDate(), rss.getTitleEx().getValue(), rss.getDescription().getValue());
				feedList.add(feed);				
			}
		} catch (Exception ex) {
            ex.printStackTrace();
            System.out.println("ERROR: "+ex.getMessage());
        }
		return feedList;
	}
    
    

}