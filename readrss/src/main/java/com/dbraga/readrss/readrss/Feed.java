package com.dbraga.readrss.readrss;

import java.util.Date;

public class Feed {
	private String url;
	private Date publishedDate;
	private String title;
	private String description;
	
	public Feed() {
		
	}
	
	public Feed(String url, Date publishedDate, String title, String description) {
		super();
		this.url = url;
		this.publishedDate = publishedDate;
		this.title = title;
		this.description = description;
	}
	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}
	public Date getPublishedDate() {
		return publishedDate;
	}
	public void setPublishedDate(Date publishedDate) {
		this.publishedDate = publishedDate;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	@Override
	public String toString() {
		return url + ", " + title + ", " + publishedDate;
	}
	
}
